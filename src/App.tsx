import './App.css'
import {MuiButton} from "./components/MuiButton.tsx";
import {MuiTextField} from "./components/MuiTextField.tsx";
import {MuiSelect} from "./components/MuiSelect.tsx";
import {MuiRadioButton} from "./components/MuiRadioButton.tsx";
import {MuiCheckbox} from "./components/MuiCheckbox.tsx";
import {MuiSwitch} from "./components/MuiSwitch.tsx";
import {MuiRating} from "./components/MuiRating.tsx";
import {MuiAutocomplete} from "./components/MuiAutocomplete.tsx";
import {MuiLayout} from "./components/MuiLayout.tsx";
import {MuiCard} from "./components/MuiCard.tsx";
import {MuiAccordion} from "./components/MuiAccordion.tsx";
import {MuiImageList} from "./components/MuiImageList.tsx";
import {MuiNavbar} from "./components/MuiNavbar.tsx";
import {MuiLink} from "./components/MuiLink.tsx";
import {MuiBreadcrumbs} from "./components/MuiBreadcrumbs.tsx";
import {MuiDrawer} from "./components/MuiDrawer.tsx";
import {MuiSpeedDial} from "./components/MuiSpeddDial.tsx";
import {MuiBottomNavigation} from "./components/MuiBottomNavigation.tsx";
import {MuiAvatar} from "./components/MuiAvatar.tsx";
import {MuiBadge} from "./components/MuiBadge.tsx";
import {MuiList} from "./components/MuiList.tsx";
import {Chip} from "@mui/material";
import {MuiTooltip} from "./components/MuiTooltip.tsx";
// import {MuiTypography} from "./components/MuiTypography.tsx";

function App() {

  return (
    <div className="App">
      {/*<MuiTypography />*/}
        <MuiButton />
        <MuiTextField />
        <MuiSelect />
        <MuiRadioButton />
        <MuiCheckbox />
        <MuiSwitch />
        <MuiRating />
        <MuiAutocomplete />
        <MuiLayout />
        <MuiCard />
        <MuiAccordion />
        <MuiImageList />
        <MuiNavbar />
            <MuiLink />
        <MuiBreadcrumbs />
        <MuiDrawer />
        <MuiSpeedDial />
        <MuiBottomNavigation />
        <MuiAvatar />
          <MuiBadge />
        <MuiList />
        <Chip />
        <MuiTooltip />
    </div>
  )
}

export default App
