import {Avatar, AvatarGroup, Stack} from "@mui/material";

export const MuiAvatar = () => {

    return (
        <Stack spacing={4}>
            <Stack direction="row" spacing={1}>
                <Avatar sx={{bgColor: 'primary.light'}}>BW</Avatar>
                <Avatar sx={{bgColor: 'success.light'}}>CK</Avatar>
                <AvatarGroup max={3}>
                    <Avatar sx={{bgColor: 'primary.light'}} alt="avatar1">BW</Avatar>
                    <Avatar sx={{bgColor: 'success.light'}}>CK</Avatar>
                    <Avatar sx={{bgColor: 'success.light'}}>CK</Avatar>
                    <Avatar sx={{bgColor: 'success.light'}}>CK</Avatar>
                </AvatarGroup>
            </Stack>
        </Stack>
    )
};