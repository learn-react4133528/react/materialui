import {Box, Divider, List, ListItem, ListItemAvatar, ListItemButton, ListItemIcon, ListItemText} from "@mui/material";
import MailIcon from "@mui/icons-material/Mail";

export const MuiList = () => {

    return (
        <Box
            sx={{width: '400px', bgcolor: '#efefef'}}
        >
            <List>
                <ListItem disablePadding>
                    <ListItemButton>
                        <ListItemIcon>
                            <ListItemAvatar>
                                <MailIcon />
                            </ListItemAvatar>
                        </ListItemIcon>
                        <ListItemText primary="List item 1" secondary="Secondary text"/>
                    </ListItemButton>
                </ListItem>
                <Divider/>
            </List>
            <List>
                <ListItemIcon>
                    <ListItemAvatar>
                        <MailIcon />
                    </ListItemAvatar>
                </ListItemIcon>
                <ListItem>
                    <ListItemText primary="List item 2" />
                </ListItem>
                <Divider/>
            </List>
            <List>
                <ListItemIcon>
                    <ListItemAvatar>
                        <MailIcon />
                    </ListItemAvatar>
                </ListItemIcon>
                <ListItem>
                    <ListItemText primary="List item 3" />
                </ListItem>
                <Divider/>
            </List>
        </Box>
    )
};