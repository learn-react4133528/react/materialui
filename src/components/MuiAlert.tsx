import {Alert, AlertTitle, Button, Stack} from "@mui/material";
import CheckIcon from "@mui/icons-material/Check";
export const MuiAlert = () => {

    return (
        <Stack spacing={2}>
            <Alert severity="error">Error</Alert>
            <Alert variant="outlined" severity="warning" onClose={() => alert('Close alert')}>
                <AlertTitle>Warning</AlertTitle>
                This is warning
            </Alert>
            <Alert variant="filled" severity="info">Info</Alert>
            <Alert variant="standard" severity="success" icon={<CheckIcon fontSize="inherit"/>}
                   action={<Button color="inherit" size="small">Undo</Button>}>
            >Success</Alert>
        </Stack>
    )
};