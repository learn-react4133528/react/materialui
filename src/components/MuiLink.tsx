import {Link, Stack, Typography} from "@mui/material";

export const MuiLink = () => {

    return (
        <Stack
            spacing={2}
            direction="row"
            m={4}
        >
            <Link
                href="#" variant="body2"
            >
                Link
            </Link>
            <Typography variant="h6">

            </Typography>
            <Link
                href="#"
                color="secondary"
                underline="hover"
            >
                Link
            </Link>
        </Stack>
    )
};