import {Autocomplete, Stack, TextField} from "@mui/material";
import {useState} from "react";

type Skill = {
    id: number;
    label: string;
}
const skills = ['HTML', 'CSS', 'JavaScript', 'React'];

const skillsOption = skills.map((skill, index) => ({
    id: index + 1,
    label: skill,
}))
export const MuiAutocomplete = () => {
    const [value, setValue] = useState<string | null>(null);
    const [skill, setSkill] = useState<Skill | null>(null);


    return (
        <Stack spacing={2} width="250px">
            <Autocomplete
                options={skills}
                renderInput={(params) => <TextField {...params} label="skills" />}
                value={value}
                onChange={(e: any, newValue: string | null) => setValue(newValue)}
                freeSolo
            />
            <Autocomplete
                options={skillsOption}
                renderInput={(params) => <TextField {...params} label="skills" />}
                value={skill}
                onChange={(e: any, newValue: Skill | null) => setSkill(newValue)}
            />
        </Stack>
    )
};