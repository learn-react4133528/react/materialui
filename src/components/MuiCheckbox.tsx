import {Box, Checkbox, FormControl, FormControlLabel, FormGroup, FormHelperText, FormLabel} from "@mui/material";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import BookmarkIcon from "@mui/icons-material/Bookmark";
import {useState} from "react";

export const MuiCheckbox = () => {
    const [acceptTnC, setAcceptTnC] = useState(false);
    const [skills, setSkills] = useState<string[]>([]);
    
    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAcceptTnC(e.target.checked)
    }
    const handleSkillChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const index = skills.indexOf(e.target.value);
        if(index === -1) {
            setSkills([...skills, e.target.value])
        } else {
            setSkills(skills.filter((skill) => skill !== e.target.value))
        }
    }

    return (
        <div>
            <Box>
                <Box>
                    <FormControlLabel
                        label="i accept terms and conditions"
                        control={<Checkbox
                            checked={acceptTnC}
                            onChange={handleChange}
                        />}
                    />
                </Box>
                <Box>
                    <Checkbox
                        icon={<BookmarkBorderIcon />}
                        checkedIcon={<BookmarkIcon />}
                        checked={acceptTnC}
                        onChange={handleChange}
                    />
                </Box>
                <Box>
                    <FormControl>
                        <FormLabel>Skills</FormLabel>
                        <FormGroup row>

                            <FormControlLabel
                                label="HTML"
                                control={<Checkbox
                                    value="html"
                                    checked={skills.includes('html')}
                                    onChange={handleSkillChange}
                                />}
                            />
                            <FormControlLabel
                                label="CSS"
                                control={<Checkbox
                                    value="css"
                                    checked={skills.includes('css')}
                                    onChange={handleSkillChange}
                                />}
                            />
                            <FormControlLabel
                                label="js"
                                control={<Checkbox
                                    value="js"
                                    checked={skills.includes('js')}
                                    onChange={handleSkillChange}
                                />}
                            />
                        </FormGroup>
                        <FormHelperText>Invalid selection</FormHelperText>
                    </FormControl>
                </Box>
            </Box>
        </div>
    )
};