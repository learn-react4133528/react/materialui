import {Box, Button, Card, CardActions, CardContent, CardMedia, Typography} from "@mui/material";

export const MuiCard = () => {

    return (
        <Box width="300px">
            <Card>
                <CardMedia
                    image="https://source.unsplash.com/random"
                    height="140"
                    component="img"
                    alt="unsplash image"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        React
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small">Share</Button>
                    <Button size="small">Learn more</Button>
                </CardActions>
            </Card>
        </Box>
    )
};