import {Box, FormControl, FormControlLabel, FormHelperText, FormLabel, Radio, RadioGroup} from "@mui/material";
import {useState} from "react";

export const MuiRadioButton = () => {
    const [value, setValue] = useState('');

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const value = e.target.value;
        setValue(value)
    }
    return (
        <Box>
            <FormControl error>
                <FormLabel id="job-experience-group-label">
                    Years of experience
                </FormLabel>
                <RadioGroup
                    name='job-expreience-group'
                    aria-labelledby="job-experience-group-label"
                    value={value}
                    onChange={handleChange}
                    row
                >
                    <FormControlLabel control={<Radio size="medium" color="secondary"/>} label="0-2" value="0-2" />
                    <FormControlLabel control={<Radio />} label="3-5" value="3-5" />
                    <FormControlLabel control={<Radio />} label="6-10" value="6-10" />
                </RadioGroup>
                <FormHelperText >Invalid Selection</FormHelperText>
            </FormControl>

        </Box>
    )
};