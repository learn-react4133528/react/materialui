import {Accordion, AccordionDetails, AccordionSummary, Typography} from "@mui/material";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import {useState} from "react";
export const MuiAccordion = () => {
    const [expanded, setExpanded] = useState<string | false>(false);

    const handleChange = (isExpanded: boolean, panel: string) => {
        setExpanded(isExpanded ? panel : false)
    }
    return (
        <div>
            <Accordion expanded={expanded === 'panel1'} onChange={(e, isExpanded) => handleChange(isExpanded, 'panel1')}>
                <AccordionSummary
                    id="panell1-header"
                    aria-controls="panel1-content"
                    expandIcon={<ExpandMoreIcon />}
                >
                    Accordion 1
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel2'} onChange={(e, isExpanded) => handleChange(isExpanded, 'panel2')}>
                <AccordionSummary
                    id="panel2-header"
                    aria-controls="panel2-content"
                    expandIcon={<ExpandMoreIcon />}
                >
                    Accordion 2
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem
                    </Typography>
                </AccordionDetails>
            </Accordion>
            <Accordion expanded={expanded === 'panel3'} onChange={(e, isExpanded) => handleChange(isExpanded, 'panel3')}>
                <AccordionSummary
                    id="panell3-header"
                    aria-controls="panel3-content"
                    expandIcon={<ExpandMoreIcon />}
                >
                    Accordion 3
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        Lorem LoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLoremLorem
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </div>
    )
};