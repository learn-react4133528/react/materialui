import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";

export const MuiTable = () => {

    return (
        <TableContainer component={Paper} sx={{maxHeight: '300px'}}>
            <Table aria-label="simple table" stickyHeader>
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell>First name</TableCell>
                        <TableCell>Last name</TableCell>
                        <TableCell>Email</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        tableData.map(row => (
                            <TableRow key={row.id} sx={{'&:last-child td, &:last-child th': {border: 0} }}>
                                <TableCell>{row.id}</TableCell>
                                <TableCell>{row.first_name}</TableCell>
                                <TableCell>{row.last_name}</TableCell>
                                <TableCell align="center">{row.email}</TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
};

const tableData = [{
    "id": 1,
    "first_name": "Wilbert",
    "last_name": "Linay",
    "email": "wlinay0@photobucket.com",
    "gender": "Genderqueer",
    "ip_address": "47.189.85.118"
}, {
    "id": 2,
    "first_name": "Daveta",
    "last_name": "Tomczykiewicz",
    "email": "dtomczykiewicz1@pinterest.com",
    "gender": "Female",
    "ip_address": "88.186.178.55"
}, {
    "id": 3,
    "first_name": "Marsiella",
    "last_name": "Rodolfi",
    "email": "mrodolfi2@friendfeed.com",
    "gender": "Female",
    "ip_address": "70.144.151.73"
}, {
    "id": 4,
    "first_name": "Tucker",
    "last_name": "Hames",
    "email": "thames3@ocn.ne.jp",
    "gender": "Male",
    "ip_address": "30.188.111.114"
}, {
    "id": 5,
    "first_name": "Fianna",
    "last_name": "Lillgard",
    "email": "flillgard4@xrea.com",
    "gender": "Female",
    "ip_address": "154.163.140.191"
}, {
    "id": 6,
    "first_name": "Gabey",
    "last_name": "McEntee",
    "email": "gmcentee5@census.gov",
    "gender": "Female",
    "ip_address": "89.100.197.181"
}, {
    "id": 7,
    "first_name": "Kev",
    "last_name": "Houndesome",
    "email": "khoundesome6@nbcnews.com",
    "gender": "Male",
    "ip_address": "86.123.25.6"
}, {
    "id": 8,
    "first_name": "Brion",
    "last_name": "Nicholson",
    "email": "bnicholson7@cisco.com",
    "gender": "Male",
    "ip_address": "176.24.19.187"
}, {
    "id": 9,
    "first_name": "Tynan",
    "last_name": "Greves",
    "email": "tgreves8@omniture.com",
    "gender": "Male",
    "ip_address": "158.14.117.33"
}, {
    "id": 10,
    "first_name": "Binny",
    "last_name": "Bordone",
    "email": "bbordone9@hhs.gov",
    "gender": "Female",
    "ip_address": "29.229.245.243"
}]