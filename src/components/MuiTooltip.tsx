import {IconButton, Tooltip} from "@mui/material";
import Deleteticon from "@mui/icons-material/Delete";

export const MuiTooltip = () => {

    return (
        <Tooltip title="Delete" placement="right" arrow enterDelay={500}>
            <IconButton>
                <Deleteticon />
            </IconButton>
        </Tooltip>
    )
};