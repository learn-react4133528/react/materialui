import {Box, Drawer, IconButton, Typography} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import {useState} from "react";

export const MuiDrawer = () => {
    const [isDrawedOpen, setIsDrawedOpen] = useState(false);

    return (
        <>
            <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="logo"
                onClick={() => setIsDrawedOpen(true)}
            >
                <MenuIcon />
            </IconButton>
            <Drawer
                anchor="left"
                open={isDrawedOpen}
                onClose={() => setIsDrawedOpen(false)}
            >
                <Box
                    p={2}
                    width="250px"
                    textAlign="center"
                    role="presentation"
                >
                    <Typography variant="h6" component="div">
                        Side panel
                    </Typography>
                </Box>
            </Drawer>
        </>
    )
};