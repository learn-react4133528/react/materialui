import {Avatar, Chip, Stack} from "@mui/material";
import FaceIcon from "@mui/icons-material/Face";

export const MuiChip = () => {

    return (
        <Stack direction="row" spacing={1}>
            <Chip label="Chip" color="primary" size="small" icon={<FaceIcon/>}/>
            <Chip
                label="Chip outlined"
                color="primary"
                size="small"
                variant="outlined"
                avatar={<Avatar>V</Avatar>}
            />
            <Chip label="Click" color="success" onClick={() => console.log('clicked')} />
            <Chip label="Delete" color="error" onClick={() => console.log('clicked')} onDelete={() => console.log('onDelete')} />
        </Stack>
    )
};